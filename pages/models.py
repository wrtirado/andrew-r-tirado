from django.db import models

# Create your models here.
# Begin CV Model
class Category(models.Model):
    cv_category = models.CharField(max_length=200)
    
    def __str__(self):
        return self.cv_category


class CV_Detail(models.Model):
    year = models.CharField(max_length=30, blank=True)
    detail = models.CharField(max_length=350)
    url = models.CharField(max_length=200, blank=True)
    owner = models.ForeignKey(Category, related_name="cv_detail", on_delete=models.CASCADE)

    def __str__(self):
        return self.detail


class Press_Reference(models.Model):
    image = models.URLField()
    title = models.CharField(max_length=50)
    snippet = models.CharField(max_length=500)
    
    def __str__(self):
        return self.title
