from django.shortcuts import render
from pages.models import Category

# Create your views here.

# Begin About view function
def show_about(request):
    context = {
        'title': 'ABOUT -',
    }
    return render(request, 'about/about.html', context)

# Begin CV view function
def show_cv(request):
    category = Category.objects.all().order_by("-id")

    context = {
        'category_list': category,
        'title': 'CV -',
    }
    return render(request, 'about/cv.html', context)

#Begin Process View Function
def show_process(request):
    context = {
        'title': 'PROCESS -',
    }
    return render(request, 'about/process.html', context)