from django.urls import path
from pages.views import (
    show_about,
    show_cv,
    show_process
)

urlpatterns = [
    path("about/", show_about, name="show_about"),
    path("about/cv/", show_cv, name="show_cv"),
    path("about/process/", show_process, name="show_process")
]