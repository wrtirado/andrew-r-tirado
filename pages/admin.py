from django.contrib import admin
from pages.models import Category, CV_Detail, Press_Reference

# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    pass

class CV_DetailAdmin(admin.ModelAdmin):
    pass

class Press_ReferenceAdmin(admin.ModelAdmin):
    pass

admin.site.register(Category, CategoryAdmin)
admin.site.register(CV_Detail, CV_DetailAdmin)
admin.site.register(Press_Reference, Press_ReferenceAdmin)
