from django.urls import path
from work.views import show_sculptures, show_installations, show_abstract, show_two_d

urlpatterns = [
    path("sculpture/", show_sculptures, name="show_sculptures"),
    path("installations/", show_installations, name="show_installations"),
    path("abstract/", show_abstract, name="show_abstract"),
    path("2d/", show_two_d, name="show_two_d"),
]