from django.db import models

# Create your models here.

# Begin Work Page Models
class Sculpture(models.Model):
    image = models.URLField()
    title = models.CharField(max_length=200)
    created_date = models.CharField(max_length=15)
    dimensions = models.CharField(max_length=200, blank=True)
    used_materials = models.CharField(max_length=300, blank=True)
    location = models.CharField(max_length=50, blank=True)

class Two_D(models.Model):
    image = models.URLField()
    title = models.CharField(max_length=200)
    created_date = models.CharField(max_length=15)
    dimensions = models.CharField(max_length=200, blank=True)
    used_materials = models.CharField(max_length=300, blank=True)
    location = models.CharField(max_length=50, blank=True)

class Installation(models.Model):
    image = models.URLField()
    title = models.CharField(max_length=200)
    created_date = models.CharField(max_length=15)
    dimensions = models.CharField(max_length=200, blank=True)
    used_materials = models.CharField(max_length=300, blank=True)
    location = models.CharField(max_length=50, blank=True)

class Abstract(models.Model):
    image = models.URLField()
    title = models.CharField(max_length=200)
    created_date = models.CharField(max_length=15)
    dimensions = models.CharField(max_length=200, blank=True)
    used_materials = models.CharField(max_length=300, blank=True)
    location = models.CharField(max_length=50, blank=True)

# END Work Page Models
