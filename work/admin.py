from django.contrib import admin
from work.models import Sculpture, Two_D, Installation, Abstract

# Register your models here.
@admin.register(Sculpture)
@admin.register(Two_D)
@admin.register(Installation)
@admin.register(Abstract)


class SculptureAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )

class Two_DAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )

class InstallationAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )

class AbstractAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )
