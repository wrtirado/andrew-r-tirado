from django.shortcuts import render
from work.models import Sculpture, Installation, Two_D, Abstract
from django.http import HttpResponse

# Begin work page view functions
def show_sculptures(request):
    sculpture_list = Sculpture.objects.all().order_by("-id")

    context = {
        'sculpture_list': sculpture_list,
        'title': 'SCULPTURES - '
    }
    return render(request, 'work/sculpture.html', context)


def show_installations(request):
    installation_list = Installation.objects.all().order_by("-id")

    context = {
        'installation_list': installation_list,
        'title': 'INSTALLATIONS - '
    }
    return render(request, 'work/installation.html', context)


def show_abstract(request):
    abstract_list = Abstract.objects.all().order_by("-id")

    context = {
        'abstract_list': abstract_list,
        'title': 'ABSTRACT - '
    }
    return render(request, 'work/abstract.html', context)


def show_two_d(request):
    two_d_list = Two_D.objects.all().order_by("-id")

    context = {
        'two_d_list': two_d_list,
        'title': '2D - '
    }
    return render(request, 'work/two-d.html', context)
# END work page view functions